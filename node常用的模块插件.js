//1、querystring——将GET请求url中的字符串信息进行转换

const querystring = require('querystring');
querystring.parse(req.url) //解析成json
querystring.stringify(str)  //转换成字符串

//2、chalk——把控制台输出信息的字符串颜色改变

var chalk=require("chalk")
console.log(`${chalk.red("今天下雨了！")}`);


//3、body-parser——将客户端通过POST方法传过来的req.body数据解析成json数据
//（依赖express模块，使用要在req.body前调用，不过要用来处理表单数据还要安装另一个插件）
//如果要获取页面表单的信息（req.body等），必须在html页面中为标签设置name属性

var express=require("express");
const bodyParser=require("body-parser");
var app=express();
app.use(bodyParser());
app.use((req,res)=>{
    console.log(req.body);
})
app.listen(80)


//4、cookie-parser——处理cookie信息

const express=require("express");
const cookieParser=require("cookie-parser")
const app=express();
app.use(cookieParser())
app.get('/',function(req,res){
    console.log("Cookies",req.cookies);
})
app.listen(80)


//5、express模块中自带的path-to-regexp模块可以通过正则表达式来匹配地址中的路径，
//存入到req.params中，以动态的访问页面

const express=require("express");
const router=express.Router();
router.get('/:data/:id',(req,res)=>{
    res.json(req.params)
})
module.express=router;


//6、svg-captcha用来生成验证码

router.get('/verfiy2.gif', function(req, res, next) {
  const cap =  svgCaptcha.create({
    size: 10,  //设置验证码长度
    noise: 10,  //设置干扰线
    color:true  //是否有颜色变化
  })
  console.log(cap.text)
  res.type('svg')
  res.end(cap.data)
});


//7、trek-captcha用来生成验证码

router.get('/verfiy.gif', function(req, res, next) {
  captcha({ size: 5, style: -1 }).then(cap=>{  //size设置验证码长度  style设置是否会变换验证码颜色
    console.log(cap.token)
    res.type('gif')
    res.end(cap.buffer)
  })
});


//8、emailjs用来通过邮箱找回密码



//9、validator验证器



//10、mongodb模块连接mongodb数据库

const MongoClient = require('mongodb').MongoClient  // 引入mongodb模块的client对象
 
const url = 'mongodb://localhost:27017';  // 添加mongodb的服务器地址
 
const dbName = 'lemon'  // 设置数据库的名称 （可以是不存在的数据库，它会自动创建）
 
function insert(collectionName,data,callback){
    MongoClient.connect(url,(err,client)=>{
        if(err) throw err;
        client.db(dbName).collection(collectionName).insert(data,callback);
        client.close();
    })
}



//11、crypto  express自带的加密模块

const crypto = require('crypto')
// 创建hash值 md5 sha1 sha256 sha512
const data =  crypto.createHash("md5").update("123456").digest('hex')
const data = crypto.createHmac("md5","zhangsan").update("123456").digest('hex') 
//createHmac的第一个参数为加密的算法，有md5 sha1 sha256 sha512等，第二个参数为加密签名，进行二次加密
console.log(data)



//12、express-session  session的认证机制离不开cookie，需要同时使用cookieParser 中间件，
//可以用来保存用户的登陆状态，免密码登陆


//13.


//14、date-format，用来格式化时间

var format = require('date-format');
var date = format('yyyyMMddhhmmssSSS', new Date());



//15、form表单文件上传模块，formidable（在html文件中的form表单中，需要
//设置enctype="multipart/form-data" 属性，）

var formidable = require('formidable');
var fs = require('fs');
 
var form = new formidable.IncomingForm();  //创建一个form表单
form.encoding='utf-8';  //设置字符编码
form.uploadDir = './upload';  //设置接收文件路径，如果路径不存在，会报错
form.keepExtensions=true;  // 默认不保持后缀名
 
 
fs.rename(oldPath,newPath,function (err) {  //结合fs模块对文件重命名
    if (err) throw err;
    res.send('上传成功');
})
form.parse(req, function(err, fields, files) { //fields：字段域   files：文件信息
    onsole.log(fields);
    console.log(files);
    var oldPath = files.file.path;   //file为form表单中type="file"的input的name属性值
    // 格式化当前时间
    var date = format('yyyyMMddhhmmssSSS', new Date());
    var newPath = './upload/' + date + files.file.name;
    fs.rename(oldPath,newPath,function (err) {
    if (err) throw err;
        res.send('上传成功');
    })
});



//16、压缩工具 html-minifier

//npm i html-minifier
var fs = require('fs');
var minify = require('html-minifier').minify;
fs.readFile('./index.htm', 'utf8', function (err, data) {
    if (err) {
        throw err;
    }
    fs.writeFile('./index_Min.html', minify(data,{removeComments: true,collapseWhitespace: true,minifyJS:true, minifyCSS:true}),function(){
        console.log('ok');
    });
});

//第一个参数为要处理的文件，第二个参数为options 默认值均为false，removeComments（去掉注释），
//collapseWhitespace（去掉空格），minifyJS（压缩html里的js（使用uglify-js进行的压缩）），
//minifyCSS （压缩html里的css（使用clean-css进行的压缩））




//17、密码加密模块

const crypto=require("crypto")
 
password = crypto.createHmac('md5','onlyyu').update(password).digest('hex');
 
//注册的时候通过加密模块加密再添加到数据库中，在登录验证的时候，同样需要先加密再进行对比验证登录




//18、使用 node-dev 可以实现热更新js代码

//npm i -g node-dev

//如果需要运行app.js文件，输入命令 node-dev app.js 即可开启热更新




//19、qs 模块  qs.parse()将URL解析成对象的形式，qs.stringify()将对象 序列化成URL的形式，以&进行拼接

var a = {name:'haha',age:10};
 
qs.stringify(a) //输出 'name=haha&age=10'
 
JSON.stringify(a) //输出 '{"a":"haha","age":10}'

