// ode-mongodb连接

const mongodbClient = require('mongodb').MongoClient;
const url = 'mongodb://locolhost:27017';
const dbName = 'test'; // 数据库名称

mongodbClient.connect(url, function(err, client){
  console.log('连接成功!');
  const db = client.db(dbName);
  const collection = db.collection('test'); // 连接到表
  // 执行mongo语句
  //......        
  client.close(); // 关闭连接  
})


//插入
const insertData = function(collection, callback) {   
    //插入数据
    const data = [{"name":'zhangsan',"age":21},{"name":'lisi',"age":22}];
    collection.insert(data, function(err, result) { 
        if(err){
            console.log('Error:'+ err);
            return;
        }     
        callback(result);
    });
  }
  mongodbClient.connect(url, function(err, client){
    console.log('连接成功!');
    const db = client.db(dbName);
    const collection = db.collection('test');
    insertData(collection, function(result){
      console.log(result);
    });
})

//查询

 const selectData = function(collection, callback) {  
    // 查询所有
    let whereStr = {"name":"张三"};
    collection.find(whereStr).toArray(function(err, result) {
      if(err){
        console.log('Error:'+ err);
        return;
      }     
      callback(result);
    });
    // 条件查询 查询参数很多，自行查阅
    collection.find({"age":{$gt:30}}).toArray(function(err, result) {
      if(err)
      {
        console.log('Error:'+ err);
        return;
      }     
      callback(result);
    });
  }


  //修改

  const updateData = function(collection, callback) {  
      const whereStr = {"name":'zhangsan'};
      const updateStr = {$set: { "age" : 100 }};
      collection.update(whereStr, updateStr, function(err, result) {
          if(err){
              console.log('Error:'+ err);
              return;
          }     
          callback(result);
      });
}

//删除

var delData = function(db, callback) {  
    //连接到表  
    var collection = db.collection('tb2');
    //删除数据
    var whereStr = {"name":'wilson001'};
    collection.remove(whereStr, function(err, result) {
      if(err)
      {
        console.log('Error:'+ err);
        return;
      }     
      callback(result);
    });
}

//存储过程

 const invokeProcData = function(db, callback) {  
      db.eval('get_tb2_count()', function(err, result) { 
          if(err)
          {
              console.log('Error:'+ err);
              return;
          }             
          callback(result);
      });
}


//分页查询

const User = require("./user.js");
function getByPager(){
    const pageSize = 5;                   //一页多少条
    const currentPage = 1;                //当前第几页
    const sort = {'logindate':-1};        //排序（按登录时间倒序）
    const condition = {};                 //条件
    const skipnum = (currentPage - 1) * pageSize;   //跳过数
 
    User.find(condition).skip(skipnum).limit(pageSize).sort(sort).exec(function (err, res) {
        if (err) {
            console.log("Error:" + err);
        }
        else {
            console.log("Res:" + res);
        }
    })
}
getByPager();



//https://wyyuan.com/2018/04/29/NODE_MONGO/#more


// 一般不原生，而是采用mongoose
